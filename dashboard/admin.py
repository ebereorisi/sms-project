from django.contrib import admin

from . import models

admin.site.register(models.School)
admin.site.register(models.YearTerm)
admin.site.register(models.ClassGroup)
admin.site.register(models.ClassSubject)
