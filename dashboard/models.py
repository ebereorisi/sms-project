from django.db import models

class School(models.Model):
    school_name = models.CharField(max_length=200)
    school_address = models.CharField(max_length=400)
    school_logo = models.ImageField(upload_to="images/")

    def __str__(self):
        return self.school_name

class YearTerm(models.Model):
    session = models.CharField(max_length=20)
    term = models.CharField(max_length=20)
    term_start = models.DateField(blank=True)
    term_end = models.DateField(blank=True)

    def __str__(self):
        return self.session + " " + self.term + " Term"


class ClassSubject(models.Model):
    subject = models.CharField(max_length=50)
    def __str__(self):
        return self.subject

class ClassGroup(models.Model):
    class_name = models.CharField(max_length=200)
    class_subjects = models.ManyToManyField(ClassSubject)

    def __str__(self):
        return self.class_name
