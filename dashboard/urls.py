from django.urls import path

from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('test-view', views.TestView.as_view(), name='test-view'),
]
