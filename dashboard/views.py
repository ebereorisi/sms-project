from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import School

class TestView(TemplateView):
	template_name = 'dashboard/test-view.html'

class HomeView(ListView):
	model = School
	template_name = 'dashboard/home.html'
