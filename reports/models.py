from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

from student.models import Student
from dashboard.models import YearTerm
from dashboard.models import ClassSubject
from dashboard.models import ClassGroup


class Report(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    year_term = models.ForeignKey(YearTerm, on_delete=models.CASCADE)

    #def __str__(self):
        #return self.student, self.year_term

    #def report_subject(ClassGroup):
        #return ClassGroup.class_subjects
    #report_subject = models.ManyToManyField(ClassGroup.class_subjects)


    #def __str__(self):
        #report_student = str(self.student)
        #return self.report_student
    #def get_absolute_url(self):
        #return reverse('detail', args=[str(self.id)])
