from django.urls import path

from . import views

urlpatterns = [
    path('create-report', views.CreateReportView.as_view(), name='create-report'),
]
