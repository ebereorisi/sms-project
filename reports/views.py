from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Report


class CreateReportView(CreateView):
	model = Report
	template_name = 'reports/create-report.html'
	#fields = ['student', 'year_term']
	fields = '__all__'
