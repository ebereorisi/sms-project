from django.db import models
from datetime import date

from dashboard.models import *

class Student(models.Model):
    name = models.CharField(max_length=200)
    date_of_birth = models.DateField(blank=True)
    class_group = models.ForeignKey(ClassGroup, on_delete=models.CASCADE)
    GENDER_CHOICES = (
        ('MALE', 'male'),
        ('Female', 'female'),
        ('OTHER', 'other'),
    )
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, default='MALE')
    home_address = models.CharField(max_length=300, blank=True)
    #Move up class_group by one value every year
    def age(self):
        today = date.today()
        return today.year - self.date_of_birth.year #- ((today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))

    def __str__(self):
        return self.name
